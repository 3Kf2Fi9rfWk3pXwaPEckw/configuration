Paint3d has a default save location that is is shown in the UI.  It is:

	%localappdata%\Packages\Microsoft.MSPaint_8wekyb3d8bbwe\LocalState\Projects

These project folders can be large.  Once edited export to ".glb" format.