.. include:: globals.rst.inc

Platforms
=========

The Scientific Software Stack is available on the following Met Office
Linux systems:

* Scientific desktop
* SPICE
* Cray HPC estate (XCE, XCF, XCS-C, XCS-O, XCS-R)
* Production Linux Servers
* Linux VDI (Linux Virtual Desktop Infrastructure)

Most Stack environments are available on all Met Office Linux systems.
A core tenet of the Scientific Software Stack is that where environments are
available on multiple systems, the contents of those environments will be 
identical on all systems.



Deployment Method & Window
--------------------------

The following section outlines the schedule window for *deployment* onto
the respective platforms.  Please be aware that although environments are
deployed according to these schedules, that does not necessarily mean that
any updates will be applied in those deployments.  This depends entirely
upon the recent activity regarding additions, revisions or fixes to
respective environments.

For the following sections the term **development** refers to environments
that are not intended for operational use.  This is essentially all of the
environments that are not named ``preproduction`` or ``production``.


Scientific Desktop, VDI & SPICE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All deployments are managed by jobs run on **els056**. RPMs are deployed on
**els056** providing all appropriate live environments. All SPICE nodes,
**eld** hosts, certain **els** hosts (including **els056**) and the VDI
have modules and environments cross mounted via SPICE shared storage.

On `els056` the following updates are scheduled, managed via a root owned AVD
cron job.

======================== ====================================================
**Name**                 **Timing**
------------------------ ----------------------------------------------------
development              Monday to Friday at 06h55
``preproduction-osXX``   Monday to Friday at 06h55
``production-osXX-X``    Monday to Friday at 06h55
======================== ====================================================

.. note:: ``preproduction`` environments are typically only deployed to the
          HPC estate.  If a ``preproduction`` environment is required on the
          SPICE estate, then please contact `AVD Support <contact>`_

These scheduled jobs update the continually changing environments across the
server cluster.



Cray HPC
^^^^^^^^

XCE & XCF
"""""""""

======================== =======================================================
**Name**                 **Timing**
------------------------ -------------------------------------------------------
development              Mon-Fri at 12:45 and 18:45
``preproduction-osXX``   Deployed well before the next Parallel Suite commences.
``production-osXX-X``    Deployed just before the next Parallel Suite commences.
======================== =======================================================


XCS-R & XCS-C
"""""""""""""

========================= =======================================================
**Name**                  **Timing**
------------------------- -------------------------------------------------------
development               Synchronised from XCE Mon-Fri at 13:45 and 19:45
``preproduction-osXX``    Deployed well before the next Parallel Suite commences.
``production-osXX-X``     Deployed just before the next Parallel Suite commences.
========================= =======================================================

We strive for production environment parity across the CRAY estate,
facilitated through the `HPC Support Team <https://metnet2.metoffice.gov.uk/team/hpc-support>`_
performing RPM deployment at our request via **ServiceNow**.  Production
deployments are a one-off.  They are not automated or on a rolling schedule, and we aim
to **retire** them through an agreed timeline with the Operational Suite
Project Manager.



Production Linux Servers
^^^^^^^^^^^^^^^^^^^^^^^^

========================  =========================================================================
**Name**                  **Timing**
------------------------  -------------------------------------------------------------------------
development               N/A
``production-osXX-X``     Any production environments must be requested via **ServiceNow**
                          for  |br| `desktop services <https://metnet2.metoffice.gov.uk/team/platforms>`_
                          to deploy the RPM.
========================  =========================================================================

All deployment for operational linux servers should be done via :term:`RPM` with
the owner of the operational servers being made aware.  See
`what operational environment to use <production_use#which-environment-to-use>`_
for more information.
