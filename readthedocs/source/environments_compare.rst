.. include:: globals.rst.inc

:orphan:


Compare Environments
--------------------

Select the two environments to compare.  See the
`Environment Browser <environments_available>`_ for explore the contents of
all environments.

.. include:: sss-browser/autogen-sss-env-list-diff-form.rst.inc





