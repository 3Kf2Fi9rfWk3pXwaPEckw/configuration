.. include:: globals.rst.inc

Package Policy
==============

The :term:`Scientific Software Stack` consists of many Python packages that are
maintained in the Open Source community.  Updates are frequently made to
these packages by the community, and if these latest updates were
automatically added to the Scientific Software Stack, then it is likely
that package dependencies could be broken and more work is then needed to
investigate and resolve these issues by the AVD Team.

Hence, a policy is required to help with, and understanding how decisions
are made for including, excluding and removing packages within the
Scientific Software Stack.


Making the Decision
-------------------

* The AVD Team will engage with users where appropriate
* Any decisions made will be visible via
  the `Requested Packages Decision Log <http://www-twiki/Gfx/SSSDecisionLog>`_
* Regular AVD Team meetings will be held to review the requested
  packages.  This will occur approximately every 2 weeks
* Users can make use of
  the `Requested Packages Decision Log <http://www-twiki/Gfx/SSSDecisionLog>`_
  log page for specific packages to add the initial justification and any
  additional comments


Guideline Requirements
----------------------

These requirements must be met in order for a package to be
considered for addition to the Scientific Software Stack:

* Must have a demonstrated broad purpose for use within the Met Office
  community, and must be suitable for addition into the Scientific Software
  Stack.
* Python3 ready.  Python2 is at **end of life 1 Jan 2020**.  As
  an organisation we should be adopting Python3
* Open source & well maintained
* Version controlled
* Testable
* Can be built with no inappropriate dependencies
* `Standard OSI approved licenses <https://opensource.org/licenses/category>`_

There may be exceptions to these requirements that will be taken into consideration.

.. warning:: Python 2.x is `END of LIFE on Jan 1st 2020 <https://www.python.org/dev/peps/pep-0373/>`_


Package Licensing
-----------------

As noted above, Scientific Software Stack packages are normally required to
have one of the standard OSI approved licenses. But this does **not**
guarantee that the package's license is approved by the
`Met Office open source policy <https://metnet2
.metoffice.gov.uk/content/met-office-policy-use-and-development-free-and
-open-source-software>`_. If un-approved, the package can still be used for
individual pieces of work, but should **not** be included in any
MO-developed software. For more information please visit the link above.


Supported Packages
------------------

The AVD Team develops and supports a core set of packages that are part
of `SciTools <https://github.com/SciTools>`_.

The packages that the AVD Team support are as follows:

* SciTools

  * iris
  * cartopy
  * nc-time-axis
  * cf_units
  * iris-grib
  * iris-agg-regrid
  * python-stratify
  * mo_pack

* Internal

  * monty

* Core dependencies

  * netcdf
  * matplotlib
  * numpy


The packages under SciTools have many dependencies that will be provisioned
as part of the Scientific Software Stack.

.. note:: All packages not listed above that are added to the stack will only
          be provisioned and not supported by the AVD Team.


Updating Packages
-----------------

Our default stance is to include the latest version of the package, when a
new environment is built.

Exceptions to this are:

* If we have pinned against a specific version of a package to ensure
  dependent packages are not broken
* If there are known security issues or suspicious code
* The development effort is too prohibitive


Removing Packages
-----------------

We cannot continually add to the Scientific Software Stack. We aim to nurture
the contents of the stack and retire unused or deprecated packages.

Examples of this:

* pep8 which was superseded by `flake8 <http://flake8.pycqa.org/en/2.5.5/>`_
* Orphaned packages after a parent package was updated.  The AVD Team will
  aim to monitor this.

Any decisions to deprecate will be visible via
the `Requested Packages Decision Log <http://www-twiki/Gfx/SSSDecisionLog>`_.  Additionally
there will be announcements of any planned removals of packages
to ensure user awareness.  Environments are immutable so any removal will
only affect future releases.


If you have any questions in regards to this policy please contact
the `AVD Team <contact>`_.
