.. include:: globals.rst.inc

FAQ
===

On this page you can find answers to commonly-asked questions that you too
might have about the Scientific Software Stack. Each FAQ is handily
categorised based on its intended primary audience.



For new starters
----------------

How does module load affect my environment? How long does it have an effect for?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``module load scitools`` will change the collection of packages that are used by
python in your current terminal.  Anything that you run from that terminal
will then use the module-loaded environment (collection of packages) instead
of those used by your default python executable.

This environment will only be applied to the specific terminal or active
shell script in which the module was loaded, so as soon as you close the
terminal of the shell scripts ends the module-loaded environment is no longer
active.


How do I run Python?
^^^^^^^^^^^^^^^^^^^^

You can run Python in several ways.  If you wanted to run a Python script
from a terminal, you would use the command ``python`` followed by the name of
the script, like this:

.. code-block:: bash

   $ python my_python_script.py

Alternatively, you can open an interactive Python session in a terminal, by
just entering the command ``python``.

See the `Best Practice <best_practice>`_ guide recommendation on how to best
run your python.

It is also possible to run IPython either as an interactive session or a
Jupyter notebook; please see the
`IPython documentation <https://ipython.readthedocs.io/en/stable/>`_ for details
and guidance.


How do I use the Scientific Software Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See the `Quick Start <getting_started>`_ and `Best Practice <best_practice>`_
guide.



For experienced python users
----------------------------

What environments are updated automatically?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See `Deployment Schedule <platforms#deployment-schedule>`_.


How can I reproduce an environment from three years ago?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please `contact us <contact>`_ if you think you need an older environment,
as there may be some issues associated with using an immutable legacy environment
in an operating system which may have changed.


How do I use the Scientific Software Stack with rose?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Inside your Rose suite, for each instance in which Python is called, you must
add a shell wrapper which activates a Stack environment before calling the Python
script.

See the `Best Practice <best_practice>`_ guide recommendation on
how to best run your python using a shell script.



How do I get a new package into the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See `Package Requests <package_requests>`_.


How do I find what version of a library is available?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See the appropriate platform in
`Environment Browser <environments_available>`_ and follow the links to
the various environments to see their **contents**.


Something just broke! How do I find out if an environment just got updated?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To find when a particular environment last changed on your platform, you will
need two pieces of information.

The **first** piece is the date that the environment got updated.  This is denoted
by the date on the label for the environment.  To find that date, navigate to
the directory ``/opt/scitools/environments`` and then list the directory
containing the environment you are using.  You will then be able to see the
date that the label changed for that description.

For example, if you wanted to check the last change date of the ``default-next``
environment, you would use these commands **[*1]**:

.. code-block:: bash

   $ cd /opt/scitools/environments
   $ ll default
   drwxr-xr-x 13 root root 4096 Mar  2  2017 2017_02_28
   drwxr-xr-x 13 root root 4096 Mar  3  2017 2017_03_02
   drwxr-xr-x 13 root root 4096 Nov 15 08:21 2017_03_02-1
   drwxr-xr-x 13 root root 4096 Apr 19  2017 2017_04_06
   drwxr-xr-x 13 root root 4096 Jun  7  2017 2017_06_07
   drwxr-xr-x 13 root root 4096 Oct 20 16:22 2017_06_07-1
   drwxr-xr-x 13 root root 4096 Nov 20 17:08 2017_06_07-2
   drwxr-xr-x 14 root root 4096 Jul 21  2017 2017_07_18
   drwxr-xr-x 14 root root 4096 Aug 22 16:59 2017_08_21
   drwxr-xr-x 14 root root 4096 Oct 12 16:48 2017_10_06
   drwxr-xr-x 15 root root 4096 Nov 15 08:22 2017_11_08
   lrwxrwxrwx  1 root root   47 Nov 20 17:08 current -> /opt/scitools/environments/default/2017_06_07-2
   lrwxrwxrwx  1 root root   45 Nov 15 08:22 next -> /opt/scitools/environments/default/2017_11_08
   lrwxrwxrwx  1 root root   47 Nov 15 08:22 previous -> /opt/scitools/environments/default/2017_03_02-1

Then you would be able to see the date on the label 'next'.  However, this
environment may not yet have been deployed to the platform you are using, so
the **second** piece of information you will need is the
`Deployment Schedule <platforms#deployment-schedule>`_.

**[*1]** The console output presented above was correct at the time the
command was run.


I am testing the new version of the Stack and something has been broken, who do I tell?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please contact the `AVD Team <contact>`_ with your issue.  If it is urgent
please ensure that you state this. Please also state the platform and
environment that you were using and include a small piece of code which can
replicate the issue.


Where should I go to find the changes made to an environment?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See the links to the _contents_ in the
`Environment Browser <environments_available>`_ page.  This shows the
manifest for each environment.


Where do you announce changes being made to the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All changes made to the Stack are announced on the
`AVD Yammer Group <https://www.yammer.com/metoffice.gov.uk/#/threads/inGroup?type=in_group&feedId=10592520>`_.  You
can `view past announcements <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_ also.


What are the implications of using the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Scientific Software Stack is a great tool for keeping your software
versions up-to-date, but this can come with its own implications which you
must be aware of.

For example:

* You will have to load a scitools module in every terminal/shell script
* Different environments have different rates of change
* Non-production environments may change
* Scripts run in a non-production environment may suddenly fail due to an
  update
* The contents of environments with the same name may differ between platforms

In order to manage Stack associated risks, please follow these
`Software Stack Best Practices <best_practice>`_.



How do I check and fix my system path variable?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You may want to check this to help fix environment problems you may be
having.  Unexpected or unwanted paths in this variable could lead to confusion
about which packages or even which Python executable is being used.

To check the variable:

On command line:

.. code-block:: bash

   echo $PATH

In Python:

.. code-block:: python

   import sys
   print(sys.path)


This will return a list of paths that are searched through for Python
executables and libraries.  The paths will be searched in the order that they
are listed, so if there are any in the list which shouldn't be there, you will
have to reset the PATH variable with only the paths you wish to use, in
prioritised order.  For example:

``export PATH=</path/to/python/executable/bin:/path/to/additional/python/library/bin:`` and so on.



How do I check for local path configuration files?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These files (identifiable by their suffix ``.pth``) contain the paths to locally
installed Python packages, and can be found in your local site-packages
folder.  For example, you would find locally installed packages for your Python 2.7
environments here:

``~/.local/lib/python2.7/site-packages``

Similarly, you would find your locally installed packages for Python 3.6
here:

``~/.local/lib/python3.6/site-packages``

You can temporarily disable these packages by moving the path configuration
files to a memorable folder (one that's called 'disabled', for example), or
permanently disable them by deleting the file completely.

Alternatively, you can invoke a Python session without using any local site
packages by using the ``-s`` flag in your Python call, like so:
``python -s``


How should I use the stack in a script?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See the `best practice <best_practice>`_ for more information.




How should I use the stack in the terminal?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See the `best practice <best_practice>`_ for more information.


How should I use the Stack in my IDE (PyCharm, Eclipse)?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This process will differ slightly for different IDEs, but will all be based
around changing your Python interpreter.  You can set your Python interpreter
as a default setting or for each separate Python script.

You will need to know the path to the Python interpreter of your chosen
environment.  This is listed under ``prepend-path``, which can be found using
the ``module display <env>`` command.

For example:

.. code-block:: bash

   $ module display scitools/default-next
   -------------------------------------------------------------------
   /usr/share/Modules/modulefiles/scitools/default-next:

   module-whatis    Scientific software stack for the 'default/next' environment.
   conflict         scitools
   prepend-path     PATH /opt/scitools/environments/default/2017_11_08/bin
   prepend-path     LD_LIBRARY_PATH /usr/lib64/atlas
   -------------------------------------------------------------------


Here are the user guides for some popular IDEs which explain how to set your
Python interpreter:

* `IntelliJ IDEA <https://www.jetbrains.com/help/idea/configuring-python-interpreter-for-a-project.html>`_
* `PyCharm <https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html>`_
* `Eclipse <http://www.pydev.org/manual_101_interpreter.html>`_

Some extra MetOffice-specific info relating to Eclipse on the scientific desktop:
`Eclipse and PyDev on the desktop <eclipse_and_pydev>`_.


How should I use the Stack for cron jobs?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You will need to make sure that your cron job is correctly configured for
preferred shell (typically bash) so that the appropriate subshell will be
initialised (as opposed to one defined by your standard configuration).

To ensure that you initialise a shell with the module-specified configuration
you can invoke your command either using a flag, like this (for bash and korn
respectively):

.. code-block:: bash

   bash -lc <command>
   ksh -lc <command>

Or you can explicitly invoke the module shell function, like this (again, for
bash and korn respectively):

.. code-block:: bash

   ./etc/bashrc; <command>
   ./etc/kshrc; <command>

Both of these options will ultimately call
``./etc/profile.d/metoffice.d/modules.sh`` to ensure correct shell configuration.


I just want to use the latest supported thing!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The latest reliable, supported environment is ``default-current``.  This
environment offers up-to-date software versions, and will be updated frequently.

If you would prefer an environment which is stable and immutable, then you may
prefer to use the most recent ``production`` environment.


For Scientific Software Stack stakeholders
------------------------------------------

When is the next release?
^^^^^^^^^^^^^^^^^^^^^^^^^

Please follow the
`AVD Yammer group <https://www.yammer.com/metoffice.gov.uk/#/threads/inGroup?type=in_group&feedId=10592520>`_
for announcements about environment releases, and the
`Deployment Schedule <platforms#deployment-schedule>`_ for the platform you
are using.


What will the next release contain?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once an environment has been released, you can view its contents by following
the relevant link from `Environment Browser <environments_available>`_.


When was the last release?
^^^^^^^^^^^^^^^^^^^^^^^^^^

You can find this information by viewing our
`previous Yammer announcements <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_.


How do environments differ across platforms?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Occasionally there will be minimal differences in environments between
platforms due to compatibility issues.

For example, some packages use files which are located on a specific
platform.  ``monty``, which is utilised by ``cartopy``, accesses OS maps which
are stored in a permissions-restricted directory on the Scientific Desktop,
so they cannot be copied or moved to another platform as this would cause
copyright infringement.  Subsequently, the recipes for the respective platforms
must reflect this difference.

Please compare links from `Environment Browser <environments_available>`_
to see any such differences for environments and platforms which you are
considering using.


For Operational Suite support teams
-----------------------------------

What is the status of the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Environments that have already been deployed will remain in an accessible
state indefinitely.  There may, however, be some occasions when additions and
updates to the Stack are suspended due to problems with the build or
deployment processes.  If this is the case, we will make an announcement on
`Yammer <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_
about it.


What changed in the last day/two days/week?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You will be able to find this information by comparing environment contents in
the relevant platform, which you can find in
`Environment Browser <environments_available>`_.


What changes are upcoming?
^^^^^^^^^^^^^^^^^^^^^^^^^^

Please consult the `decision log <http://www-twiki/Gfx/SSSDecisionLog>`_
to see a summary of packages which have been accepted as additions to the Stack.


How do I perform an emergency fix?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have a problem with some software in the Stack and you need to
temporarily replace it with a customized package (maybe a local package
containing a bugfix or workaround), we recommend that you add the directory
containing your local package to your ``PYTHONPATH`` variable. Please see our
`Extending Environments <extending_environments>`_ guide for details.


What environments are being used in Operational Suites?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Operational environments are labelled ``osXX`` (potentially followed by a build
number).  See `Environment Browser <environments_available>`_ to view
the contents of the current operational environments.


For suite owners
----------------

What is expected of me as a user of the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As the Scientific Software Stack represents a variety of working environments,
it is your responsibility as a user to embed a log in your suite of the exact
environment(s) used to run it.

Please see the `Software Stack Best Practices <best_practice>`_ for
guidance.


How do I get new packages included in the Stack?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See `Package Requests <package_requests>`_.


How do I request an existing package get updated?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please contact the `AVD Team <contact>`_ with your request.


How do I subscribe to service announcements?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All changes made to the Stack are announced on the
`AVD Yammer group <https://www.yammer.com/metoffice.gov.uk/#/threads/inGroup?type=in_group&feedId=10592520>`_.  You
can `view past announcements <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_ also.


Are the available stacks in parity between production versions and those on the Scientific Desktop?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Scientific Desktop environments get updated frequently, whereas production
environments are immutable.  To compare package version in different
environments, please view environment contents in
`Environment Browser <environments_available>`_.


Why does my code have errors when run on the Supercomputer/SPICE (matplotlib backend)?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some users have had difficulties with
plotting because the default matplotlib backend does not function correctly
in a **headless** environment (meaning with no **DISPLAY**).

Any use of the default backend ``Qt5Agg`` must be
avoided on the Supercomputer, as it can cause crashes (exceptions or
segfaults).  Instead use the ``Agg`` backend.

You can select a backend in your Python code immediately after importing
matplotlib.

For example :

::

   import matplotlib as mpl;
   mpl.use('Agg')

For more information see https://matplotlib.org/3.2.1/api/matplotlib_configuration_api.html?highlight=use#matplotlib.use

You can also use a user config or an environment variable, but the above
method is generally better because it is explicitly set in the code that it
affects.  For more information see
https://matplotlib.org/3.2.1/tutorials/introductory/usage.html#backends

For normal interactive use on the Science Desktop (VDI), you generally want
to see plot windows on the **DISPLAY**. For this, suitable choices are
``TkAgg`` or ``Qt5Agg``.  If you do nothing else, you get ``Qt5Agg`` as that
is the default.

However on the Supercomputer or SPICE there is no display. This is known as a
known as a **headless** platform.  In that case, the above are not good
choices, and can cause problems even if you only ever write images to disk
``plt.savefig`` rather than display plots ``plt.show``

.. note:: On **headless** platforms, you should always use ``mpl.use('Agg')``.
          This is a safe choice, and also more efficient.
