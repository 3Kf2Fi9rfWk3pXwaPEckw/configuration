.. include:: globals.rst.inc

Cheat Sheet
===========

In order to help users understand the basics of the Scientific Software Stack
improve on the cheat sheet, please contact `AVD Support <contact>`_ with any
suggestions or post suggestions on one of our Yammer channels.

We have two cheat sheets, a normal one and a mini one that shows less
information for easier reading

* PDF version at :download:`sss-cheat-sheet.pdf <sss-cheat-sheet.pdf>`
* PDF mini version at :download:`sss-cheat-sheet-mini.pdf <sss-cheat-sheet-mini.pdf>`

Additionally there a PNG copies of the cheat sheets previewed below.
:guilabel:`Right click` on the image and select
:guilabel:`Open image in new tab` or :guilabel:`View Image` to see in
full resolution.

.. figure:: sss-cheat-sheet.png
   :align: center

   **Scientific Software Stack Cheat Sheet**


.. figure:: sss-cheat-sheet-mini.png
   :align: center

   **Scientific Software Stack Cheat Sheet (mini)**
