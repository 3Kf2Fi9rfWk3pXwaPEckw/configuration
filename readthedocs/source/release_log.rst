.. include:: globals.rst.inc

Release Log
===========

Releases will also be shared on `AVD Announcements on Yammer`_.
For a full list of currently available environments please see the
:ref:`environment_browser`.

If you wish to recreate an old environment that is no longer available in the
Scientific Software Stack, you can use the :term:`manifest` to create your
own environment.  For more information contact the AVD Team.

.. note:: This was created on **04/10/2020** and is updated manually at this time
          but in future will be replaced with an automated process to pull
          releases from the appropriate repository.

.. _AVD Announcements on Yammer: https://web.yammer.com/main/topics/eyJfdHlwZSI6IlRvcGljIiwiaWQiOiIyODM2MDUzOSJ9


.. list-table:: Table of Release History (most recent at top)
   :align: left
   :widths: 10 10 10 10

   * - **Date**
     - **Environment**
     - **Python Version**
     - **Notes**

   * - 2020-11-19
     - experimental-current
     - python-3.6.6
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-experimental-2020_11_19/env.manifest>`__

   * - 2020-10-17
     - experimental-current
     - python-3.6.6
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-experimental-2020_10_17/env.manifest>`__

   * - 2020-10-12
     - default-next
     - python-3.6.10
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-default-2020_10_12/env.manifest>`__

   * - 2020-05-29
     - production-os44-1
     - python-3.6.10
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production-2020_05_29/env.manifest>`__

   * - 2020-03-10
     - experimental-current
     - python-3.6.10
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-experimental-2020_03_10/env.manifest>`__

   * - 2019-07-26
     - production_legacy-os43-2
     - python-2.7.16
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production_legacy-2019_07_26/env.manifest>`__

   * - 2019-07-26
     - production-os43-2
     - python-3.6.8
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production-2019_07_26/env.manifest>`__

   * - 2019-06-12
     - production_legacy-os43-1
     - python-2.7.16
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production_legacy-2019_06_12/env.manifest>`__

   * - 2019-06-12
     - production-os43-1
     - python-3.6.8
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production-2019_06_12/env.manifest>`__

   * - 2019-02-27
     - default_legacy-current
     - python-2.7.15
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-default_legacy-2019_02_27/env.manifest>`__

   * - 2019-02-27
     - default-current
     - python-3.6.8
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-default-2019_02_27/env.manifest>`__

   * - 2019-02-27
     - default-next
     - python-3.6.8
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-default-2019_02_27/env.manifest>`__

   * - 2019-01-08
     - production-os42-1
     - python-3.6.8
     - `manifest <https://bitbucket.org/metoffice/environments/src/env-production-2019_01_08/env.manifest>`__

   * - 2018-10-17
     - production_legacy-os42-1
     - python-2.7.12
     - `manifest <https://bitbucket.org/metoffice/Scidesktop-environments/src/env-production_legacy-2018_10_17-1/env.manifest>`__

   * - 2018-05-22
     - default-previous
     - python-2.7.12
     - `manifest <https://bitbucket.org/metoffice/Scidesktop-environments/src/env-default-2018_05_22-1/env.manifest>`__

   * - 2018-05-22
     - default_legacy-previous
     - python-2.7.12
     - `manifest <https://bitbucket.org/metoffice/Scidesktop-environments/src/env-default_legacy-2018_05_22-1/env.manifest>`__

   * - 2018-02-01
     - production-os41-1
     - python-2.7.12
     - `manifest <https://bitbucket.org/metoffice/Scidesktop-environments/src/env-production-2018_02_01/env.manifest>`__
