.. include:: globals.rst.inc

Best Practice
=============

Using the Scientific Software Stack offers many benefits, but can also present
certain risks.  Those risks can be mitigated by following these guidelines.

.. tip:: If is strongly advised to **not** to use the ``module load``
         commmand in your ``.profile``, ``.bashrc`` or ``.kshrc`` file.  This
         is not best practice.

Use a shell Script where possible
---------------------------------

Where possible wrap the Python code that uses the Scientific Software Stack in
a Shell script. This will allow you to easily and consistently log details of
the environment that you are using.

The example below is a simple bash script that:

* Loads a specific stack environment
* Sets the ``PYTHONPATH`` to include any custom Python pachages
* Displays the stack details using environment variables.  Alternatively you
  could use the ``module help scitools`` or ``module display scitools``
* Calls a Python script

Using this approach will mitigate clashes with any settings in your current
Linux ``shell`` shell.


.. code-block:: bash
   :linenos:
   :caption: call-mypython.bash
   :emphasize-lines: 7,8,26

   #!/bin/bash

   #
   # load the instance of the stack you want, in this case scitools-default.
   # also show some info using the help option.
   #
   module load scitools
   module help scitools

   #
   # need to update the PYTHONPATH to include yor project code?
   #
   export PYTHONPATH=~me/python/my-project:${PYTHONPATH}

   #
   # display useful environment variables
   #
   echo "LD_LIBRARY_PATH = ${LD_LIBRARY_PATH}"
   echo "PYTHONPATH = ${PYTHONPATH}"
   echo "SSS_ENV_DIR = ${SSS_ENV_DIR}"
   echo "SSS_TAG_DIR = ${SSS_TAG_DIR}"

   #
   # call your python program
   #
   python my-python.py

.. tip:: Ideally capture the **stdout** and **stderr** of the scripts to a
         **log file** to allow for easy debugging and a log of the environment
         used at the time.  This is of particular note if using of the
         ``default-*`` scitools environments as these will change over time.

.. warning:: If you invoke a stack environment within a Python script by
             using a **hashbang**, any **dependant environments will not
             be loaded**.  For this reason, it is recommended that you
             instead load the stack environment using ``module``.  See
             `best practice <best_practice>`_.


Use a shell script to setup an interactive Python session
---------------------------------------------------------

If you want to use Python interactively it is still recommended to use a
``shell`` script to setup your environment to ensure consistency.

The above example bash script can be reused but without the call to Python.

Note the ``.`` before the call to the script, this allows any ``bash``
Linux environment variables to be set in your current session.  If the
``.`` is not include when the scripts ends the Linux environments variables
will be lost.

.. code-block:: bash
  :linenos:
  :caption: my-env-setup.bash

  $ . my-env-setup.bash

  ----------- Module Specific Help for 'scitools/default-current' ---------------------------

  Scientific software stack for the 'default/current' environment.

  This environment is available via:
     - the symbolic link /opt/scitools/environments/default/current, or
     - the base directory /net/project/ukmo/scitools/opt_scitools/environments/default/2019_02_27

  Note that, the environment symbolic link points to the environment base
  directory.

  As new software is deployed to the scientific software stack, this environment
  symbolic link is automatically updated to point to the appropriate environment
  base directory.

  For documentation, see http://www-avd/sci/software_stack/
  LD_LIBRARY_PATH = /project/ukmo/rhel7/fortran/opt/intel/composer_xe_2017.7.259/compilers_and_libraries_2017.7.259/linux/compiler/lib/intel64:/project/ukmo/rhel7/fortran/opt/intel/composer_xe_2017.7.259/compilers_and_libraries_2017.7.259/linux/mkl/lib/intel64:/project/ukmo/rhel7/fortran/opt/intel/composer_xe_2017.7.259/compilers_and_libraries_2017.7.259/linux/mpi/intel64/lib:/project/ukmo/rhel7/fortran/opt/intel/composer_xe_2017.7.259/compilers_and_libraries_2017.7.259/linux/daal/lib/  intel64_lin:/project/ukmo/rhel7/fortran/opt/intel/composer_xe_2017.7.259/debugger_2017/libipt/intel64/lib:/project/ukmo/rhel7/fortran/opt/pgi/linux86-64/16.10/lib:/project/ukmo/rhel7/LaTeX/packages/poppler/0.61.1/lib64:/project/ukmo/rhel7/LaTeX/packages/tiff/4.0.9/lib64:/project/ukmo/rhel7/LaTeX/packages/pixman/0.34.0/lib:/project/ukmo/rhel7/LaTeX/packages/openjpeg/2.3.0/lib:/project/ukmo/rhel7/LaTeX/packages/lcms/2.9/lib:/project/ukmo/rhel7/fortran/opt/gfortran/packages/gcc/8.1.0/lib64:/opt/ukmo/share/lib:/project/ukmo/rhel7/R/R-3.6.0/lib64/R/lib:/project/ukmo/rhel7/R/R-3.6.0/lib:/usr/lib/oracle/12.2/client64/lib
  PYTHONPATH = ~me/python/my-project:
  SSS_ENV_DIR = /opt/scitools/environments/default/current
  SSS_TAG_DIR = /opt/scitools/environments/default/2019_02_27

  $ python



Know your ``environment`` contents
----------------------------------

* Look in the **conda-meta** directory for
  information on package revisions e.g., once an environment is loaded, refer
  to ``${SSS_TAG_DIR}/conda-meta``.


Know which stack to use
-----------------------

* Be aware of which stack
  `environment <environments#environment-description>`_ you are
  using and whether it is appropriate for the task you are performing (i.e.,
  consider the update frequency and contents of the environment).


Know your PYTHONPATH
--------------------

* Use the `PYTHONPATH <extending_environments>`_
  method where possible to `extend <extending_environments>`_ your Python
  environment.  Ideally set this in a script so it is easy repeated.
