.. include:: globals.rst.inc

Troubleshooting
===============

Being in control of your environment is great, and being able to choose which
environment you use and add extra packages to it can get a little messy and
lead to issues down the line.

Keep a record
-------------

* `Keep a record <extending_environments#the-pythonpath-variable>`_ of where
  your Python executable is finding its packages by printing ``sys.path``
  to a log file.


Check inherited settings
-------------------------

* Check your `local path configuration files <faq#how-do-i-check-for-local-path-configuration-files>`_
  (files ending in ``.pth``) which may be
  overriding packages in your chosen environment.  Please see the
  `FAQ <faq>`_ for guidance.


* Check the command that calls
  Python.  Does it include any command line options that may ignore environment
  variables? For example ``python -Es``.  Run ```man python`` at your command line
  for more information.

* Check your ``.bashrc``, ``.kshrc`` or
  ``.profile`` for any alterations to environment variables such as ``PATH``,
  ``PYTHONPATH``, or ``LD_LIBRARY_PATH``.  If these are not regularly checked, it is easy to lose track
  of which variables are being set automatically.


Check you Python code
---------------------

* Check that the ``sys.path`` is not being manipulated within your Python code.
  This is **not** good practice and can cause issues.
