#!/usr/bin/env bash

# conda execute
# env:
#   - conda-build-all
# channels:
#   - http://www-avd/conda/cray_tools
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

#
# Note that, use of all "conda" sub-commands are sourced from the
# version bundled within the conda-execute temporary environment.
#

# Conda-build-all needs absolute paths to directories.
RECIPES_DIR=$(readlink -f ${1})
SOURCE_DIR=$(readlink -f ${2})

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

export HOST_TYPE='cray'
source ${SCRIPT_DIR}/env_setup.sh

CONDA_BUILD_ROOT=${CRAY_WORKDIR}/root/
PLATFORM_BUILD_ROOT=${CONDA_BUILD_ROOT}/linux-64

# Create a fresh directory for the conda-build root.
[ -d ${CONDA_BUILD_ROOT} ] && rm -rf ${CONDA_BUILD_ROOT}
mkdir -p ${PLATFORM_BUILD_ROOT}

#
# conda-build automatically calls svn to update the source of a svn
# based recipe. This is not necessary as we already have an uptodate
# cache AND the svn version on the cray is older (and incompatible)
# with the svn version used to generate the cache from the desktop.
# This work-around simply replaces the svn binary with a nop binary
# to circumvent CRAY connectivity and svn version issues.
#
CONDA_BUILD_ROOT_BIN=${CONDA_BUILD_ROOT}bin
mkdir ${CONDA_BUILD_ROOT_BIN}
DUMMY_SVN=${CONDA_BUILD_ROOT_BIN}/svn
echo "#!/usr/bin/env bash" > ${DUMMY_SVN}
chmod u+x ${DUMMY_SVN}
export PATH=${CONDA_BUILD_ROOT_BIN}:${PATH}

# Link directories containing the sources in the conda-build root.
ln -s ${SOURCE_DIR}/* ${CONDA_BUILD_ROOT}/

export CONDARC="${CRAY_WORKDIR}/condarc"
cat <<EOF > ${CONDARC}
channels:
    - ${CHANNEL_URL}

default_channels:
    - ${CHANNEL_URL}

show_channel_urls: True

add_pip_as_python_dependency: False

conda-build:
    root-dir: ${CONDA_BUILD_ROOT}
EOF

echo -e "\n${CONDARC}:"
cat ${CONDARC}
echo

# Index the conda-build root.
conda index ${PLATFORM_BUILD_ROOT}

# When it comes to building, store any new built packages in a clean directory
# so that we can see what was new.
[ -d ${NEW_DISTRIBUTIONS_DIR} ] && rm -rf ${NEW_DISTRIBUTIONS_DIR}
mkdir -p ${NEW_DISTRIBUTIONS_DIR}

# A real workaround until we resolve https://github.com/conda/conda-build/commit/3dddeaf3cf5e85369e28c8f96e24c2dd655e36f0#commitcomment-14652618
export CONDA_PY=27

# Numpy version needs setting, even though it isn't actually used!
export CONDA_NPY=18

# As per the comment in https://github.com/SciTools/iris/issues/1876 it is
# useful to have the LOCALE to UTF-8.
export LC_ALL="en_GB.UTF-8"

# Branding environment variable for the Python build.
export python_branding="| Met Office - SciTools |"

conda build-all \
    ${RECIPES_DIR} \
    --inspect-channel ${CHANNEL_URL} \
    --artefact-directory ${NEW_DISTRIBUTIONS_DIR} \
    --matrix-conditions "python 2.7.*"
