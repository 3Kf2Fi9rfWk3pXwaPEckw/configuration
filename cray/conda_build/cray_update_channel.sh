#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

export HOST_TYPE='cray'
source ${DATADIR}/conda_bld_cray/env_setup.sh

#=============================================================================
echo -e '\nTASK: Conda build all'

# The shell script to execute.
SCRIPT=${CRAY_WORKDIR}/cray_build.sh

# The entry-point to conda-execute.
EXECUTE=~avd/live/conda-execute/bin/conda-execute

# In order to access the CDN channel via http we must *NOT* use the proxy.
# Without disabling it, https://www-avd/conda will not be reachable.
unset http_proxy
unset https_proxy

# Execute the script within a conda-execute (cached) environment.
export CONDA_ENVS_PATH=${CRAY_WORKDIR}/conda/envs
mkdir -p ${CONDA_ENVS_PATH}
${EXECUTE} -vf ${SCRIPT} ${CRAY_WORKDIR}/recipes ${CRAY_WORKDIR}/source_cache

#=============================================================================
echo -e '\nTASK: Upload to channel'

TARBALLS="${NEW_DISTRIBUTIONS_DIR}/*.tar.bz2"
if [ $(ls -1 ${TARBALLS} 2>/dev/null | wc -l) -gt 0 ]; then
    echo -e "\tUploading $(ls -1 ${TARBALLS} | wc -l) build artefacts ..."
    for PACKAGE in $(ls -1 ${TARBALLS}); do
        echo -e "\t- $(basename ${PACKAGE})"
    done
    rsync --ignore-existing --remove-source-files \
        ${TARBALLS} \
        ${CHANNEL_HOST}:${CHANNEL_ROOT}
    export UPLOAD=True
else
    echo -e "\tNo build artefacts were generated to upload to the channel."
fi

#=============================================================================
echo -e '\nTASK: Reindex channel'

if [ -n  "${UPLOAD+1}" ]; then
    ssh -o BatchMode=yes ${CHANNEL_HOST} <<'EOF'
export CONDA_ENVS_PATH=${LOCALTEMP}/conda_cray/envs
mkdir -p ${CONDA_ENVS_PATH}
export EXECUTE=~avd/live/conda-execute/bin/conda-execute
${EXECUTE} -vf ${LOCALDATA}/conda_bld_cray/index_channel.sh
EOF
    echo "Updated"
else
    echo -e "\tThe channel does not require indexing."
    echo "Unchanged"
fi
