#!/usr/bin/env python

# conda execute
# env:
#   - jinja2
# channels:
#   - http://www-avd/conda/cray_tools
# run_with: python

import argparse
import os

from jinja2 import Environment


MODULEFILE_AGNOSTIC = """#%Module1.0
#
# scitools - scientific software stack for the '{{ name }}/{{ label }}' environment
#

set ENV_NAME "{{ name }}-{{ label }}"
set LINK_DIR "{{ env_dir }}/{{ name }}/{{ label }}"
set BASE_DIR [exec readlink -f $LINK_DIR ]
set HELP "Scientific software stack for the '{{ name }}/{{ label }}' environment."
set EXPERIMENTAL_ENVS {"experimental/current"}

proc ModulesHelp { } {
    global ENV_NAME
    global LINK_DIR
    global BASE_DIR
    global HELP
    global ALL_MODULE_DEPS

    puts stderr $HELP
    puts stderr ""
    puts stderr "This environment is available via:"
    puts stderr "   - the symbolic link $LINK_DIR, or"
    puts stderr "   - the base directory $BASE_DIR"
    puts stderr ""
    puts stderr "Note that, the environment symbolic link points to the environment base"
    puts stderr "directory."
    puts stderr ""
    puts stderr "As new software is deployed to the scientific software stack, this environment"
    puts stderr "symbolic link is automatically updated to point to the appropriate environment"
    puts stderr "base directory."
    puts stderr ""
    puts stderr "For documentation, see http://www-avd/sci/software_stack/"
}

module-whatis $HELP
conflict scitools um_tools
prepend-path PATH $BASE_DIR/bin
setenv SSS_ENV_DIR $LINK_DIR
setenv SSS_ENV_NAME $ENV_NAME
setenv SSS_TAG_DIR $BASE_DIR
if {"{{ label }}" in $EXPERIMENTAL_ENVS} {
    setenv PROJ_LIB $LINK_DIR/share/proj
}
"""

VERSION = """#%Module1.0
#
# scitools - scientific software stack default environment
#

set ModulesVersion "{{ default }}"
"""

ENV_DEFAULT = 'default-current'
ENV_AGNOSTIC = ['experimental-current', 'default-next', 'preproduction-os44']


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create modulefile for given '
                                                 'labelled environments.')
    parser.add_argument('env_labels', nargs='+',
                        help='The paths of the environments to create '
                             'modulefiles for.')
    parser.add_argument('-d', '--directory',
                        help='The base directory to contain all of '
                        'the modulefiles.')

    args = parser.parse_args()

    base_dir = args.directory

    template_agnostic = Environment().from_string(MODULEFILE_AGNOSTIC)
    template_version = Environment().from_string(VERSION)
    rendered_version = template_version.render(default=ENV_DEFAULT)

    for env_label in args.env_labels:
        env_dir = os.path.dirname(os.path.dirname(env_label))

        if base_dir is None:
            base_dir = os.path.join(os.path.dirname(env_dir), 'modulefiles')

        env_names = env_label.split(os.path.sep)[-2:]
        fname = '-'.join(env_names)
        name = env_names[0]
        label = env_names[1]

        if fname in ENV_AGNOSTIC:
            modulefile_path = os.path.join(base_dir, fname)
            rendered_modulefile = template_agnostic.render(name=name, label=label, env_dir=env_dir)

            if not os.path.exists(base_dir):
                os.makedirs(base_dir)

            with open(modulefile_path, 'w') as fo:
                fo.write(rendered_modulefile)

            if fname == ENV_DEFAULT:
                version_path = os.path.join(base_dir, '.version')
                with open(version_path, 'w') as fo:
                    fo.write(rendered_version)
