#!/usr/bin/env bash

# Purpose:
#   Cron job that will rsync deployed environments from a
#   source host (rsync pull) to a target host (rsync push)
#   via this local host.

#=============================================================================

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Check the availability of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)

# The source host (rsync pull)
PULL_HOST=xcel00
# The target host (rsync push)
PUSH_HOST=xcslr0

# Set up LOGFILE for output of this script.
LOGDIR=${SCRATCH}/logs
LOGFILE=${LOGDIR}/cray_rsync.${PUSH_HOST}.$(date +%Y-%m-%d-%H%M%S).log

# Now rsync from the pull host to the push host.
${SCRIPT_DIR}/cray_rsync.sh ${PULL_HOST} ${PUSH_HOST} >${LOGFILE} 2>&1
