channels:
 - https://metoffice.jfrog.io/metoffice/api/conda/conda-agnostic
 - https://metoffice.jfrog.io/metoffice/api/conda/conda-main
env:
 - python 3.6.*
 - xarray
 - dask
 - distributed
