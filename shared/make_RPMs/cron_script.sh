#!/bin/bash


PROG=$(basename ${0} .sh)


usage() {
    echo -e "\nusage: ${PROG} [-h] PLATFORM\n"
    echo -e "Build conda recipe platform specific RPMs and populate the associated platform RPM repository.\n"
    echo    "positional arguments:"
    echo -e "  PLATFORM      the target platform [${PLATFORMS}]\n"
    echo    "optional arguments:"
    echo -e "  -h, --help    show this help message and exit\n"

    exit 0
}


oops() {
    local EMSG="${1}"

    echo -e "\n${PROG}: ${EMSG}\n"

    exit 1
}


# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Define valid platforms.
declare -A VALID
VALID[cray]='cray'
VALID[rhel6]='rhel6'
PLATFORMS=$(echo "${!VALID[@]}" | sed "s/ /|/g")

# Sanity check the command line arguments.
[ ${#} -ne 1 ] && usage
[ "${1}" == "-h" -o "${1}" == "--help" ] && usage
PLATFORM=$(echo "${1}" | tr [A-Z] [a-z])
if [ -z "${VALID[${PLATFORM}]}" ]
then
    EMSG="Require a valid PLATFORM argument, got \"${1}\", expected [${PLATFORMS}]."
    oops "${EMSG}"
fi

# Define the log file.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOG_FILE=${LOG_DIR}/create_rpms_${PLATFORM}.$(date +%Y-%m-%d-%H%M%S).log

# Configure the local condarc.
export CONDARC="${LOCALTEMP}/conda/make_rpms_${PLATFORM}/condarc"
mkdir -p $(dirname ${CONDARC})
cat <<EOF >${CONDARC}

add_pip_as_python_dependency: False
ssl_verify: False

EOF

# Execute the RPM script.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda/make_rpms_${PLATFORM}/envs
EXECUTE=~avd/live/conda-execute/bin/conda-execute
unbuffer ${EXECUTE} -vf ${SCRIPT_DIR}/rpm_script.sh ${PLATFORM} >${LOG_FILE} 2>&1

RETURNCODE=${?}
# Count instances of the string "Unchanged" at the end of the log file.
# If the count is 0 then something has changed and we need to send an email.
UNCHANGED=$(tail -5 ${LOG_FILE} | grep -ic "^unchanged$")

if [ ${RETURNCODE} -ne 0 ]
then
    echo
    echo "RPM Build: Failed on ${HOSTNAME} exited with [${RETURNCODE}] - check log file \"${LOG_FILE}\"."
    echo
    cat ${LOG_FILE}
elif [ ${UNCHANGED} -eq 0 ]
then
    echo
    echo "RPM Build: Changed on ${HOSTNAME} - check log file \"${LOG_FILE}\"."
    echo
    cat ${LOG_FILE}
fi
