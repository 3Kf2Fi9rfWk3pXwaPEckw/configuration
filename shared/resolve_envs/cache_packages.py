import argparse
import os
import sys

from conda.models.channel import prioritize_channels
from conda.exports import fetch_index
from conda.models.dist import Dist
import os
import requests

try:
    # Python3...
    from urllib.parse import urlparse
except ImportError:
    # Python2...
    from urlparse import urlparse


ART_NETLOC = "metoffice.jfrog.io"
ART_PATH_PREFIX_CONDA = "/metoffice/api/conda/"
ART_PATH_PREFIX_DOWNLOAD = "/metoffice/api/download/"


def main(fname_manifest, api_user, api_key):
    # Channel index cache.
    index_by_channel = {}

    # Parse the environment manifest.
    with open(fname_manifest, 'r') as fi:
        manifest = [line.strip().split("\t") for line in fi]

    count = len(manifest)
    width = len(str(count))

    try:
        for i, (channel, package) in enumerate(manifest):
            msg = ("[{:0{width}}/{:0{width}}] Caching package {!r} from "
                   "channel {!r}")
            print(msg.format(i+1, count, package, channel, width=width))

            # Only process Artifactory channels.
            url_parts = urlparse(channel)
            if url_parts.netloc != ART_NETLOC or \
                    not url_parts.path.startswith(ART_PATH_PREFIX_CONDA):
                print("\tSkipping non-artifactory package.")
                continue

            # Inject the API user and key into the channel URLs...
            api_channel = '{}://{}:{}@{}{}'.format(url_parts.scheme, api_user,
                                               api_key, url_parts.netloc,
                                               url_parts.path)

            api_channels = prioritize_channels([api_channel])

            # Fetch and cache the channel index.
            index = index_by_channel.get(api_channel, None)
            if index is None:
                index = fetch_index(api_channels, use_cache=False)
                index_by_channel[api_channel] = index

            # Retrieve the index-record of the package from the channel index.
            channel_name, _ = api_channels[api_channel]
            dist = Dist.from_string(package, channel_override=channel_name)
            record = index[dist]

            # Sanity check the index-record package URL for artifactory.
            record_url_parts = urlparse(record.url)
            expected_api_netloc = "{}:{}@{}".format(api_user, api_key,
                                                    ART_NETLOC)
            assert record_url_parts.netloc == expected_api_netloc
            assert record_url_parts.path.startswith(ART_PATH_PREFIX_CONDA)

            # Replace artifactory API conda path prefix with the download
            # path prefix.
            api_path = record_url_parts.path.replace(ART_PATH_PREFIX_CONDA,
                                                     ART_PATH_PREFIX_DOWNLOAD,
                                                     1)
            api_url = "{}://{}{}".format(record_url_parts.scheme,
                                         record_url_parts.netloc,
                                         api_path)

            response = requests.get(api_url)
            response.raise_for_status()

    except requests.HTTPError as err:
        emsg = "Error caching package {!r} from channel {!r}: {}"
        print(emsg.format(package, channel, err))
        return 1

    return 0


if __name__ == '__main__':
    description = "Cache packages in environment manifest."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('manifest',
                        help='the filename of the environment manifest.')
    parser.add_argument('--api_user', '-u', action='store',
                        help='the Artifactory API username.')
    parser.add_argument('--api_key', '-k', action='store',
                        help='the Artifactory API key.')
    args = parser.parse_args()

    exit(main(args.manifest, args.api_user, args.api_key))
