Name: SciTools-SciDesktop-patch
Version: 1.0
Release: 1
Summary: Perform SciTools desktop patching

License: BSD 3
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}

Requires: atlas


%description
Perform custom patching of the scientific desktop to support SciTools.


%prep
rm -rf %{buildroot}


%clean
rm -rf %{buildroot}


%post
# Run this scriplet on first time install or upgrade only.
# Ref: https://docs.fedoraproject.org/en-US/packaging-guidelines/Scriptlets/#_syntax
if [ ${1} -eq 1 ] || [ ${1} -eq 2 ]; then
  #
  # Patch: support for atlas el6 v3.8 to el7 v3.10 short-term work-around.
  #
  ATLAS=%{_libdir}/atlas
  if [ -d ${ATLAS} ]; then
    ATLAS_SLIB=${ATLAS}/libsatlas.so
    if [ -L ${ATLAS_SLIB} ]; then
      LINKS="libatlas.so libatlas.so.3"
      for TARGET in ${LINKS}; do
        # Add missing atlas shared library links.
        [ ! -L ${ATLAS}/${TARGET} ] && ln -s ${ATLAS_SLIB} ${ATLAS}/${TARGET}
      done
    fi

    ATLAS_TLIB=${ATLAS}/libtatlas.so
    if [ -L ${ATLAS_TLIB} ]; then
      LINKS="libptcblas.so libptcblas.so.3 libptf77blas.so libptf77blas.so.3"
      for TARGET in ${LINKS}; do
        # Add missing atlas shared library links.
        [ ! -L ${ATLAS}/${TARGET} ] && ln -s ${ATLAS_TLIB} ${ATLAS}/${TARGET}
      done
    fi
  fi
  # Always ensure a successful exit status of 0.
  # This is required in order to avoid the post scriplet failing in the
  # situation when we're upgrading and the symbolic links already exist, which
  # will result in an exit status of 1 from the last command above.
  : # noop - to ensure exit status of 0
fi


%postun
# Execute this scriplet on removal only.
if [ ${1} -eq 0 ]; then
  #
  # Patch: support for atlas el6 v3.8 to el7 v3.10 short-term work-around. 
  #
  ATLAS=%{_libdir}/atlas
  if [ -d ${ATLAS} ]; then
    ATLAS_SLIB=${ATLAS}/libsatlas.so
    if [ -L ${ATLAS_SLIB} ]; then
      LINKS="libatlas.so libatlas.so.3"
      for TARGET in ${LINKS}; do
        # Remove the patched atlas shared library links.
        [ -L ${ATLAS}/${TARGET} ] && rm -f ${ATLAS}/${TARGET}
      done
    fi

    ATLAS_TLIB=${ATLAS}/libtatlas.so
    if [ -L ${ATLAS_TLIB} ]; then
      LINKS="libptcblas.so libptcblas.so.3 libptf77blas.so libptf77blas.so.3"
      for TARGET in ${LINKS}; do
        # Remove the patched atlas shared library links.
        [ -L ${ATLAS}/${TARGET} ] && rm -f ${ATLAS}/${TARGET}
      done
    fi
  fi
fi


%files


%changelog
* Thu Feb 14 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.0-1
- Added requires atlas and post on upgrade

