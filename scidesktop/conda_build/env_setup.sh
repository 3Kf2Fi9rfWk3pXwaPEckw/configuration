#!/usr/bin/env bash

# We define a location for the root of our build activities.
WORKDIR=${LOCALDATA}/conda_bld

# The location of the conda channel that we are building with.
CHANNEL_NAME="RedHat.x86_64"
CHANNEL_URL=http://www-avd/conda/${CHANNEL_NAME}
CHANNEL_ROOT=/project/avd/live/conda/${CHANNEL_NAME}
PLATFORM_ROOT=${CHANNEL_ROOT}/linux-64/

# The location for the conda-build working directory.
export CONDA_BUILD_ROOT=${WORKDIR}/root/

# When it comes to building, store any new recipes in a clean directory so
# that we can see what was new.
if [ -z "${NEW_DISTRIBUTIONS_DIR}" ]; then
    export NEW_DISTRIBUTIONS_DIR=${WORKDIR}/new_distributions
    [ -d ${NEW_DISTRIBUTIONS_DIR} ] && rm -rf ${NEW_DISTRIBUTIONS_DIR}
    mkdir -p ${NEW_DISTRIBUTIONS_DIR}
fi

