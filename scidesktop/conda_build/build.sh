#!/usr/bin/env bash

# conda execute
# env:
#  - cmake
#  - conda 4.3.*
#  - conda-build-all
#  - conda-build <3
#  - jinja2
#  - python
#  - requests <2.19
# channels:
#  - conda-forge
#  - defaults
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Get the directory of this script.
export SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)
 
source ${SCRIPT_DIR}/env_setup.sh

RECIPES_DIR=${1}

# Configure conda to sensible defaults, and enable the appropriate channels.
export CONDARC="${WORKDIR}/condarc"
cat <<EOF >${CONDARC}

channels:
    - file://${CHANNEL_ROOT}

default_channels:
    - file://${CHANNEL_ROOT}
 
show_channel_urls: True
 
add_pip_as_python_dependency: False

allow_softlinks: False

ssl_verify: False

conda-build:
    root-dir: ${CONDA_BUILD_ROOT}

EOF

# XXX: We need to align CONDA_NPY with the numpy recipe version about to be
#      built to ensure a successful build that avoids complaints about
#      ``numpy x.x`` type recipes.
#
#      Actually, conda-build-all should auto-detect and set CONDA_NPY for us.
#      Require to raise and issue or PR against conda-build-all to fix this
#      asap.
export CONDA_NPY=18

# In order to support recipes that define a run_test.py, but don't depend on
# Python themselves, we define a CONDA_PY that uses a Python we know we have.
# This does not influence the build matrix, and we can still build packages
# against python 3!
export CONDA_PY=27

# Branding environment variable for the Python build.
export python_branding="| Met Office - SciTools |"

# BUILD THEM!
time conda build-all \
    ${RECIPES_DIR} \
    --inspect-channel file://${CHANNEL_ROOT} \
    --artefact-directory ${NEW_DISTRIBUTIONS_DIR} \
    --matrix-conditions "python 2.7.*"
